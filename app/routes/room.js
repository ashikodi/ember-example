import Ember from 'ember';

export default Ember.Route.extend({
  model: function(params) {
    let hotelId = params.hotel_id;
    let roomId = params.room_id;
    let query = {
      hotelId: hotelId,
      roomId: roomId
    };
    console.log(2, query);
    
    return this.store.query('hotel', query);
  }
  
});
