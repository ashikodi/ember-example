import DS from 'ember-data';

export default DS.Transform.extend({
  deserialize(value) {
    let coordinates =  {
          latitude: value.latitude,
          longitude: value.longitude
        };
    return coordinates;  
  },

  serialize(value) {
    let location =  {
      latitude: value.latitude,
      longitude: value.longitude
    };
    return location;
  },
});
