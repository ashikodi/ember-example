import Model from 'ember-data/model';
import attr from 'ember-data/attr';
import { belongsTo, hasMany } from 'ember-data/relationships';

export default Model.extend({
  contentProvider: belongsTo('content-resource', {polymorphic: true}),
  code: attr('string'),
  name: attr('string'),
  description: attr('string'),
  items: hasMany('item', {async: false})
});
