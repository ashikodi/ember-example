import Model from 'ember-data/model';
import { belongsTo, hasMany } from 'ember-data/relationships';

export default Model.extend({
  hotel: belongsTo('hotel', {async: false}),
  thumbnailImage: belongsTo('media-info', {async: false, inverse: 'thumbnailImage'}),
  images: hasMany('media-info', {async: false, inverse: 'images'}),
  videos: hasMany('media-info', {async: false, inverse: 'videos'})
});
