import ContentResource from './content-resource';
import attr from 'ember-data/attr';
import { belongsTo, hasMany } from 'ember-data/relationships';

export default ContentResource.extend({
  name: attr('string'),
  description: attr('string'),
  award: attr('number'),
  isPreferential: attr('boolean'),
  location: belongsTo('location', {async: false}),
  rooms: hasMany('room', {async: false}),
  links: belongsTo('link', {async: false})
});
