import ContentResource from './content-resource';
import attr from 'ember-data/attr';
import { belongsTo, hasMany } from 'ember-data/relationships';

export default ContentResource.extend({
  hotel: belongsTo('hotel', {async: false}),
  rph: attr('number'),
  name: attr('string'),
  description: attr('string'),
  mealPlan: attr('string'),
  category: attr('string'),
  lodging: attr('string'),
  minPax: attr('number'),
  maxPax: attr('number'),
  quantityAvailable: attr('number'),
  isAvailable: attr('boolean'),
  rates: hasMany('rate', {async: false}),
  cancellationPolicy: hasMany('cancellation-policy', {async: false}),
  links: belongsTo('link', {async: false}),
  group: attr('string')
});
