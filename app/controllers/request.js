import Ember from 'ember';

export default Ember.Controller.extend({
  actions: {
    makeRequest: function() {
      var query = {
        hotel_id: this.get('hotelId'),
        room_id: this.get('roomId')
      };
      if(Ember.isEmpty(query.hotel_id) || Ember.isEmpty(query.room_id)) {
        return this.get('flashMessages').danger('Fill in all details');
      }
      return this.transitionToRoute(`/hotels/${query.hotel_id}/rooms/${query.room_id}`);
    }
  }
});
